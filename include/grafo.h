#pragma once

/**
 * @author Jorge Muñoz Taylor
 * @date 29/02/2020
 * 
 * @file grafo.h
 * @brief Se declara la clase grafo y sus métodos.
 */ 

#include <iostream>
#include <vector>
#include <lemon/list_graph.h>

using namespace std;
using namespace lemon;

/**
 * @class grafo
 * @brief Contiene los algoritmos de búsqueda en anchitud, profundidad y de prim.
 */
class grafo
{
    public:

        /**
         * @brief Recorre el grafo por niveles, imprime los nodos iniciando por la raíz y pasando por cada nivel.
         * @param graph Estructura que contiene todo el grafo con sus vértices.
         * @param NODO El nodo de inicio.
         */
        void anchitud ( ListGraph &graph, ListGraph::Node NODO );

        /**
         * @brief Recorre el grafo en profundidad, inicia por la raíz y pasa por cada nodo.
         * @param graph Estructura que contiene todo el grafo con sus vértices.
         * @param NODO El nodo de inicio.
         */
        void profundidad ( ListGraph &graph, ListGraph::Node NODO );

        /**
         * @brief Recorre el grafo según el peso de las aristas, probablemente imprima el recorrido con menor peso.
         * @param graph Estructura que contiene todo el grafo con sus vértices.
         * @param NODO El nodo de inicio.
         * @param cost El costo de todas las aristas.
         */
        void prim ( ListGraph &graph, ListGraph::Node NODO, ListGraph::EdgeMap<int> &cost );
};