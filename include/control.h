#pragma once

/**
 * @author Jorge Muñoz Taylor
 * @date 29/02/2020
 * 
 * @file control.h
 * @brief Se declaran los métodos de la clase control.
 */ 

/**
 * @class control
 * @brief Contiene los grafos que se usarán como prueba.
 */
class control
{
    public:

        /**
         * @brief Contiene la lógica principal del programa.
         */
        control ();

        /**
         * @brief Contiene el grafo 1 que usará para probar los algoritmos.
         */        
        void grafo1 ();

        /**
         * @brief Contiene el grafo 2 que usará para probar los algoritmos.
         */
        void grafo2 ();
};