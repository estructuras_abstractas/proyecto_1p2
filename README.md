# Proyecto 1 parte 2: Grafos en C++


## Integrante
```
Jorge Munoz Taylor
```

## Como compilar el proyecto
Una vez descargado el código, ubíquese en la carpeta donde está el código:
```
>>cd {PATH}/proyecto_1p2
```

Por último ejecute el make:
```
>>make
```

## Ejecutar el programa
Simplemente:
```
./proyecto12
```


## Ejecutar el test
Se probarán los algoritmos para los grafos dados de 2 formas: ejecutando únicamente make, o sino:
```
make test
```

## PARA Doxygen
Ubíquese en la carpeta donde está el código:
```
>>cd {PATH}/proyecto_1p2
```

Para compilar el doxygen:
```
>>doxygen Doxyfile
```

Para generar el archivo PDF:
```
>>cd DOCS/latex
>>make
```
Esto generará el archivo PDF con la documentación generada por doxygen. Para verlo:
```
Doble clic al archivo refman.pdf dentro de DOCS/latex
```

## Dependencias

Debe tener instalado CMAKE y MAKE en la máquina :
```
>>sudo apt-get install build-essential
>>sudo apt-get install cmake
```
(El makefile verificará e instalará todo lo siguiente)

También doxygen y latex:
```
>>sudo apt install doxygen
>>sudo apt install doxygen-gui
>>sudo apt-get install graphviz
>>sudo apt install texlive-latex-extra
>>sudo apt install texlive-lang-spanish
```

Por último debe tener la biblioteca LEMON instalada, para ello siga las instrucciones dadas en el siguiente enlace:

```
http://lemon.cs.elte.hu/pub/tutorial/a00016.html
```