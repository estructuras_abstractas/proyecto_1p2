/**
 * @author Jorge Muñoz Taylor
 * @date 29/02/2020
 * 
 * @file control.cpp
 * @brief Se definen los métodos de la clase control.
 */ 

#include <iostream>

#include <lemon/list_graph.h>
#include "../include/grafo.h"
#include "../include/control.h"

using namespace std;
using namespace lemon;


control::control ()
{
    this->grafo1();

    this->grafo2();
}   


void control::grafo1 ()
{
    ListGraph graph1; // GRAFO 1
    ListGraph::Node NODO; // Nodo inicial

    // Graph nodes
    ListGraph :: Node a = graph1 . addNode () ;
    ListGraph :: Node b = graph1 . addNode () ;
    ListGraph :: Node c = graph1 . addNode () ;
    ListGraph :: Node d = graph1 . addNode () ;
    ListGraph :: Node e = graph1 . addNode () ;
    ListGraph :: Node f = graph1 . addNode () ;
    ListGraph :: Node g = graph1 . addNode () ;
    ListGraph :: Node h = graph1 . addNode () ;
    ListGraph :: Node i = graph1 . addNode () ;
    // Graph edges
    ListGraph::Edge ab = graph1.addEdge(a, b); 
    ListGraph::Edge ah = graph1.addEdge(a, h); 
    ListGraph::Edge bc = graph1.addEdge(b, c); 
    ListGraph::Edge bh = graph1.addEdge(b, h); 
    ListGraph::Edge cd = graph1.addEdge(c, d); 
    ListGraph::Edge cf = graph1.addEdge(c, f); 
    ListGraph::Edge ci = graph1.addEdge(c, i); 
    ListGraph::Edge de = graph1.addEdge(d, e); 
    ListGraph::Edge df = graph1.addEdge(d, f); 
    ListGraph::Edge ef = graph1.addEdge(e, f); 
    ListGraph::Edge fg = graph1.addEdge(f, g); 
    ListGraph::Edge gh = graph1.addEdge(g, h); 
    ListGraph::Edge gi = graph1.addEdge(g, i); 
    ListGraph::Edge hi = graph1.addEdge(h, i);
    // Edges costs
    ListGraph :: EdgeMap <int > cost ( graph1 ) ;
    cost[ab] = 4;
    cost[ah] = 8;
    cost[bc] = 8; 
    cost[bh] = 11; 
    cost[cd] = 7; 
    cost[cf] = 4; 
    cost[ci] = 2; 
    cost[de] = 9; 
    cost[df] = 14; 
    cost[ef] = 10; 
    cost[fg] = 2; 
    cost[gh] = 1; 
    cost[gi] = 6; 
    cost[hi] = 7;

    NODO = graph1.nodeFromId (0);

    grafo gra;

    cout << endl << "======================================" << endl;
    cout <<         "                GRAFO 1" << endl;
    cout << "======================================" << endl;
    gra.anchitud ( graph1, NODO );

    gra.profundidad ( graph1, NODO );

    gra.prim ( graph1, NODO, cost );
}


void control::grafo2 ()
{
    ListGraph graph2; // GRAFO 2
    ListGraph::Node NODO; // Nodo inicial

    ListGraph::Node a = graph2.addNode(); 
    ListGraph::Node b = graph2.addNode();
    ListGraph::Node c = graph2.addNode(); 
    ListGraph::Node d = graph2.addNode(); 
    ListGraph::Node e = graph2.addNode(); 
    ListGraph::Node f = graph2.addNode(); 
    ListGraph::Node g = graph2.addNode(); 
    ListGraph::Node h = graph2.addNode(); 
    ListGraph::Node i = graph2.addNode(); 
    ListGraph::Node j = graph2.addNode(); 
    ListGraph::Edge af = graph2.addEdge(a, f); 
    ListGraph::Edge ah = graph2.addEdge(a, h); 
    ListGraph::Edge ai = graph2.addEdge(a, i); 
    ListGraph::Edge bf = graph2.addEdge(b, f); 
    ListGraph::Edge bg = graph2.addEdge(b, g); 
    ListGraph::Edge bh = graph2.addEdge(b, h); 
    ListGraph::Edge bj = graph2.addEdge(b, j); 
    ListGraph::Edge cf = graph2.addEdge(c, f); 
    ListGraph::Edge ce = graph2.addEdge(c, e); 
    ListGraph::Edge dg = graph2.addEdge(d, g); 
    ListGraph::Edge dj = graph2.addEdge(d, j); 
    ListGraph::Edge ef = graph2.addEdge(e, f); 
    ListGraph::Edge ej = graph2.addEdge(e, j); 
    ListGraph::Edge gh = graph2.addEdge(g, h); 

    ListGraph::EdgeMap <int> cost(graph2); 
    cost[af] = 343; 
    cost[ah] = 1435; 
    cost[ai] = 464; 
    cost[bf] = 879; 
    cost[bg] = 954; 
    cost[bh] = 811; 
    cost[bj] = 524; 
    cost[cf] = 1054; 
    cost[ce] = 1364; 
    cost[dg] = 433; 
    cost[dj] = 1053; 
    cost[ef] = 1106; 
    cost[ej] = 766; 
    cost[gh] = 837;

    NODO = graph2.nodeFromId (0);

    grafo gra;

    cout << endl << "======================================" << endl;
    cout <<         "                GRAFO 2" << endl;
    cout << "======================================" << endl;
    gra.anchitud ( graph2, NODO );

    gra.profundidad ( graph2, NODO );

    gra.prim ( graph2, NODO, cost );
}