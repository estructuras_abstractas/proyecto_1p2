/**
 * @author Jorge Muñoz Taylor
 * @date 29/02/2020
 * 
 * @file main.cpp
 * @brief Incluye la función principal del programa.
 */ 

#include <iostream>
#include "../include/control.h"

#define PROGRAMA_EXITO 0

int main ()
{
    control pruebas;   

    return PROGRAMA_EXITO;
}