/**
 * @author Jorge Muñoz Taylor
 * @date 29/02/2020
 * 
 * @file grafo.cpp
 * @brief Se definen los métodos de la clase grafo.
 */ 

#include <queue>
#include <stack>
#include "../include/grafo.h"

using namespace std;


void grafo::anchitud ( ListGraph &graph, ListGraph::Node NODO )
{    
    queue<int> neighbours;
    vector<int> visitados;
    ListGraph::Node temp;
    bool fue_visitado;

    //Guarda nodo a en la cola
    neighbours.push (graph.id(NODO));
    visitados.push_back (graph.id(NODO));

    while( !neighbours.empty()  )
    {
        temp = graph.nodeFromId ( neighbours.front() );
    
        neighbours.pop();

        //Recorremos los vertices hasta hallar los que estan conectados al nodo temp
        for(ListGraph::EdgeIt it(graph); it != INVALID; ++it)
        {
            fue_visitado = false;
            //Compara el nodo izq de la arista con el nodo
            if( graph.id(graph.u(it)) == graph.id(temp) ) 
            {     
                //Busca si el nodo ya a sido visitado     
                for ( int i=0; i < visitados.size(); i++ )
                {
                    if (visitados[i] == graph.id ( graph.v(it) ) && fue_visitado == false )
                    {
                        fue_visitado = true;
                        i = visitados.size();
                    }
                }

                if ( fue_visitado==false)
                {
                    neighbours.push ( graph.id(graph.v(it)) );
                    visitados.push_back ( graph.id(graph.v(it)) );
                }
            }


            //Compara el nodo derecho de la arista con el nodo
            else if ( graph.id(graph.v(it)) == graph.id(temp) ) 
            {      
                //Busca si el nodo ya a sido visitado   
                for ( int i=0; i < visitados.size(); i++ )
                {
                    if (visitados[i] == graph.id ( graph.u(it) ) && fue_visitado == false )
                    {
                        fue_visitado = true;
                        i = visitados.size();
                    }
                }
      
                if (fue_visitado==false)
                {
                    neighbours.push ( graph.id(graph.u(it)) );
                    visitados.push_back ( graph.id(graph.u(it)) );
                }
            }

        }//Fin de for

    }//Fin de while


    cout << endl << "-> Anchitud: [ ";

    for ( int i=0; i < visitados.size(); i++ )
        cout << visitados[i] << " ";
        
    cout << "]" << endl << endl;
}


void grafo::profundidad ( ListGraph &graph, ListGraph::Node NODO )
{
    stack<int> neighbours;
    vector<int> visitados;
    ListGraph::Node temp;
    bool fue_visitado;

    //Agrega origen a la pila
    neighbours.push (graph.id(NODO));
    visitados.push_back (graph.id(NODO));

    while ( !neighbours.empty() )
    {
        temp = graph.nodeFromId ( neighbours.top() );

        neighbours.pop();

        //Recorremos los vertices hasta hallar los que estan conectados al nodo temp
        for(ListGraph::EdgeIt it(graph); it != INVALID; ++it)
        {
            fue_visitado = false;
            //Compara el nodo izq de la arista con el nodo
            if( graph.id(graph.u(it)) == graph.id(temp) ) 
            {     
                //Busca si el nodo ya a sido visitado     
                for ( int i=0; i < visitados.size(); i++ )
                {
                    if (visitados[i] == graph.id ( graph.v(it) ) && fue_visitado == false )
                    {
                        fue_visitado = true;
                        i = visitados.size();
                    }
                }

                if ( fue_visitado==false)
                {
                    neighbours.push ( graph.id(graph.v(it)) );
                    visitados.push_back ( graph.id(graph.v(it)) );
                }
            }


            //Compara el nodo derecho de la arista con el nodo
            else if ( graph.id(graph.v(it)) == graph.id(temp) ) 
            {
                //Busca si el nodo ya a sido visitado    
                for ( int i=0; i < visitados.size(); i++ )
                {
                    if (visitados[i] == graph.id ( graph.u(it) ) && fue_visitado == false )
                    {
                        fue_visitado = true;
                        i = visitados.size();
                    }
                }
      
                if (fue_visitado==false)
                {
                    neighbours.push ( graph.id(graph.u(it)) );
                    visitados.push_back ( graph.id(graph.u(it)) );
                }
            }

        }//Fin de for

    }//Fin de while


    cout << endl << "-> Profundidad: [ ";

    for ( int i=0; i < visitados.size(); i++ )
        cout << visitados[i] << " ";
        
    cout << "]" << endl << endl;
}


void grafo::prim ( ListGraph &graph, ListGraph::Node NODO, ListGraph::EdgeMap<int> &cost )
{
    vector<int> visitados;
    
    ListGraph::Node temp;
    bool fue_visitado;
    int cant_nodos = 0;
    int peso_arista;
    int suma_pesos = 0;
    int nuevo_nodo;
    int nodo_actual;

    //Agrega origen a la pila
    visitados.push_back (graph.id(NODO));

    for(ListGraph::NodeIt it(graph); it != INVALID; ++it)
        cant_nodos++;

    cout << endl << "-> PRIM: " << endl << endl;

    //Termina cuando ya se visitaron todos los nodos
    while ( visitados.size() != cant_nodos )
    {
        //Reinicia el conteo del peso menor
        peso_arista = 0;

        //Se recorren todos los nodos visitados en
        for (int i=0; i<visitados.size(); i++)
        {
            temp = graph.nodeFromId ( visitados[i] );

            //Recorremos los vertices hasta hallar los que estan conectados al nodo temp
            for(ListGraph::EdgeIt it(graph); it != INVALID; ++it)
            {
                fue_visitado = false;
                //Compara el nodo izq de la arista con el nodo
                if( graph.id(graph.u(it)) == graph.id(temp) ) 
                {       
                    //Busca si el nodo ya a sido visitado     
                    for ( int j=0; j < visitados.size(); j++ )
                    {
                        if (visitados[j] == graph.id ( graph.v(it) ) && fue_visitado == false )
                        {
                            fue_visitado = true;
                            j = visitados.size();
                        }
                    }

                    //Si el nodo no ha sido visitado revisa el peso de la arista asociado a el.
                    if ( fue_visitado==false)
                    {
                        if ( peso_arista == 0 )
                        {
                            nuevo_nodo = graph.id(graph.v(it));
                            peso_arista = cost[it];
                            nodo_actual = graph.id(temp);
                        }
                        else if ( cost[it] < peso_arista )
                        {
                            nuevo_nodo = graph.id(graph.v(it));
                            peso_arista = cost[it];
                            nodo_actual = graph.id(temp);
                        }
                    }
        
                }


                //Compara el nodo derecho de la arista con el nodo
                else if ( graph.id(graph.v(it)) == graph.id(temp) ) 
                {  
                    //Busca si el nodo ya a sido visitado
                
                    for ( int j=0; j < visitados.size(); j++ )
                    {
                        if (visitados[j] == graph.id ( graph.u(it) ) && fue_visitado == false )
                        {
                            fue_visitado = true;
                            j = visitados.size();
                        }
                    }
        
                    if (fue_visitado==false)
                    {
                        if ( peso_arista == 0)
                        {
                            nuevo_nodo = graph.id(graph.u(it));
                            peso_arista = cost[it];
                            nodo_actual = graph.id(temp);
                        }
                        else if ( cost[it] < peso_arista )
                        {
                            nuevo_nodo = graph.id(graph.u(it));
                            peso_arista = cost[it];
                            nodo_actual = graph.id(temp);
                        }
                    }
                }

            }//Fin de for

        }//Fin de for

        suma_pesos = suma_pesos + peso_arista;
        visitados.push_back (nuevo_nodo);

        cout << "   ( " << nodo_actual << ", " << nuevo_nodo << " )";
        cout << "   con un peso de " << peso_arista << endl;
    }//Fin de while

    cout << endl << "   Peso total: "<< suma_pesos << endl << endl;
}