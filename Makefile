ALL: packages comp doxygen test

packages:
	@echo "**************************************************************************"
	@echo "** Se instalarán los paquetes necesarios para que funciones el programa **"
	@echo "**************************************************************************"
	@sleep 1s

	sudo apt-get update -y
	sudo apt-get install -y lemon

	sudo apt install g++ -y
	sudo apt-get install build-essential -y
	sudo apt install doxygen -y
	sudo apt install doxygen-gui -y
	sudo apt-get install graphviz -y
	sudo apt install texlive-latex-extra -y
	sudo apt install texlive-lang-spanish -y
	sudo apt-get install libsfml-dev -y
	sudo apt autoremove -y

	@echo " "
	@echo "-> Paquetes instalados"


comp:
	@echo "**************************************************************************"
	@echo "***********           Se generará el ejecutable            ***************"
	@echo "**************************************************************************"
	@sleep 1s

	mkdir -p bin build docs

	g++ -o ./build/grafo.o -c ./src/grafo.cpp
	g++ -o ./build/control.o -c ./src/control.cpp
	g++ -o ./build/main.o -c ./src/main.cpp

	g++ -o ./bin/proyecto12 ./build/main.o ./build/grafo.o ./build/control.o

	@echo " "
	@echo "-> Ejecutable generado"

doxygen:
	@echo "**************************************************************************"
	@echo "************      Generando el archivo refman.pdf          ***************"
	@echo "**************************************************************************"
	@sleep 1s

	mkdir -p docs
	doxygen Doxyfile
	make -C ./docs/latex

	@echo " "
	@echo "-> Archivo refman.pdf generado  "

test:
	@echo "**************************************************************************"
	@echo "************         Test para los algoritmos              ***************"
	@echo "**************************************************************************"
	
	@mkdir -p bin build docs
	@g++ -o ./build/grafo.o -c ./src/grafo.cpp
	@g++ -o ./build/control.o -c ./src/control.cpp
	@g++ -o ./build/main.o -c ./src/main.cpp
	@g++ -o ./bin/proyecto12 ./build/main.o ./build/grafo.o ./build/control.o 
	
	./bin/proyecto12

	@echo " "
	@echo "-> Finalizaron los test"
